#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
#ifndef _MSC_VER
void fopen_s(FILE **f, const char *fn, const char *mode){
	*f = fopen(fn, mode);
}
#define fscanf_s fscanf
#define fprintf_s fprintf
#endif
double Mstd[1111][1111], Mout[1111][1111];

double doCheck(const char * fJ, const char * fO){
	FILE * fJudge, *fOut;
	fopen_s(&fJudge, fJ, "r");
	fopen_s(&fOut, fO, "r");
	double diff = 0;
	int n, m, f;
	fscanf_s(fJudge, "%*d%d%d%d%*d", &n, &m, &f);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			fscanf_s(fJudge, "%lf", &Mstd[i][j]);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			fscanf_s(fOut, "%lf", &Mout[i][j]);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			diff += pow(Mstd[i][j] - Mout[i][j], 2);
	if (fJudge) fclose(fJudge);
	if (fOut) fclose(fOut);
	return diff;
}

int main(int argc, char** argv){
	double S = doCheck(argv[3], argv[2]);
	FILE *fResult; fopen_s(&fResult, argv[5], "w");
	FILE *fJudge; fopen_s(&fJudge, argv[3], "r");
	FILE *fLog; fopen_s(&fLog, argv[6], "w");
	int id,m1;  fscanf_s(fJudge, "%d%*d%*d%*d%d", &id,&m1);
	if (fJudge) fclose(fJudge);
	double result = 0;
	if (id == 0){
		if (S < 10)
			result = 10;
		else if (S < 20)
			result = 5;// +(8 - 5) * (80.0 - (S - 20.0)) / (80.0);
		else if (S < 40)
			result = 2;
		else if (S < 80)
			result = 1;
		else if (S < 150)
			result = 0;
		else result = 0;
	} else {
		if (S < 10)
			result = 10;
		else if (S < 20)
			result = 5;// +(8 - 5) * (80.0 - (S - 20.0)) / (80.0);
		else if (S < 40)
			result = 3;
		else if (S < 80)
			result = 2;
		else if (S < 150)
			result = 1;
		else result = 0;
	}
	if (m1 <= 5){
		if (fabs(S) <= 0.1)
			result = 10;
		else if (fabs(S) <= 0.5)
			result = 5;
		else result = 0;
	}
	fprintf_s(fResult, "%d\n", (int)result);
	fprintf_s(fLog, "%lf\n", S);
	if (fResult) fclose(fResult);
	if (fLog) fclose(fLog);
}