#include <cstdio>
#include <cstdlib>
#include <vector>
#include <cstring>

#ifndef _MSC_VER
void fopen_s(FILE **f, const char *fn, const char *mode){
	*f = fopen(fn, mode);
}
#define fscanf_s fscanf
#define fprintf_s fprintf
#endif
using namespace std;

const int MaxN = 1000 * 10;

vector<int> vOut[10], vStd[10];
int clsOut[MaxN], clsStd[MaxN];
int diff[10][10];

bool hit[MaxN];

int calc(const vector<int> &v1, const vector<int> &v2){
	int d = 0;
	for (int i = 0; i < (int)v2.size(); i++)
		hit[v2[i]] = true;
	for (int i = 0; i < (int)v1.size(); i++)
		if (!hit[v1[i]])
			d++;
	for (int i = 0; i < (int)v2.size(); i++)
		hit[v2[i]] = false;
	return d;
}

double doChecker(const char *fJ, const char *fO, FILE * fLog = stderr){
	FILE * fJudge, *fOut;
	fopen_s(&fJudge, fJ, "r");
	fopen_s(&fOut, fO, "r");
	int n, K; fscanf_s(fJudge, "%d%d", &K, &n);
	if (!(K >= 1 && K <= 10)){
		fprintf_s(fLog, "Incorrect k in line: %d file: %s.\n", 1, fJ);
		if (fJudge) fclose(fJudge);
		if (fOut) fclose(fOut);
		return 0;
	}
	for (int i = 0; i < n; i++){
		int k; fscanf_s(fOut, "%d", &k);
		if (!(k >= 1 && k <= K)){
			fprintf_s(fLog, "Incorrect N_i in line: %d file: %s.\n", i + 1, fO);
			if (fJudge) fclose(fJudge);
			if (fOut) fclose(fOut);
			return 0;
		}
		clsOut[i] = k - 1;
		vOut[k - 1].push_back(i);
	}
	for (int i = 0; i < n; i++){
		int k; fscanf_s(fJudge, "%*lf%*lf%d", &k);
		if (!(k >= 1 && k <= K)){
			fprintf_s(fLog, "Incorrect N_i in line: %d file: %s.\n", i + 2, fJ);
			if (fJudge) fclose(fJudge);
			if (fOut) fclose(fOut);
			return 0;
		}
		clsStd[i] = k - 1;
		vStd[k - 1].push_back(i);
	}

	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++){
		// diff[i][j] is different between vOut[i] and vStd[j]. 
		diff[i][j] += calc(vOut[i], vStd[j]);
		diff[i][j] += calc(vStd[j], vOut[i]);
		}
	int d = 0;
	for (int i = 0; i < n; i++){
		d += diff[clsOut[i]][clsStd[i]];
	}
	if (fJudge) fclose(fJudge);
	if (fOut) fclose(fOut);
	return 1-(d / ((double)n * n));
	
}

int main(int argc, char** argv){
	FILE *fResult, *fLog;
	fopen_s(&fResult, argv[5], "w");
	fopen_s(&fLog, argv[6], "w");
	double rate = doChecker(argv[3], argv[2], fLog);
	fprintf_s(fLog, "Rate: %.4f\n", rate);
	if (rate >= 0.965)
		fprintf_s(fResult, argv[4]);
	else if (rate >= 0.945)
		fprintf_s(fResult, "4");
	else if (rate >= 0.9)
		fprintf_s(fResult, "1");
	if (fResult) fclose(fResult);
	if (fLog) fclose(fLog);
	//~ system("pause");
	return 0;
}
