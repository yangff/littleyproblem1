#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cassert>
#include <random>
#include <ctime>

using namespace std;

struct Matrix {
	int N, M;
	double **val, *mem;
	Matrix(int N, int M, double val = 0);
	Matrix(const Matrix &v);
	struct LineOperator{
		double * val; int M;
		LineOperator(int M, double * val) :val(val), M(M){ ; }
		double & operator [] (int key){
			assert(key >= 0 && key < M);
			return val[key];
		}

		void operator += (const double v){
			for (int i = 0; i < M; i++)
				val[i] += v;
		}

		void operator -= (const double v){
			for (int i = 0; i < M; i++)
				val[i] -= v;
		}

		void operator *= (const double v){
			for (int i = 0; i < M; i++)
				val[i] *= v;
		}

		void operator /= (const double v){
			for (int i = 0; i < M; i++)
				val[i] /= v;
		}
		Matrix ToMat() {
			Matrix m(1, M);
			for (int i = 0; i < M; i++)
				m.val[0][i] = val[i];
			return m;
		}
	};

	LineOperator operator [] (int key){
		assert(key >= 0 && key < N);
		return LineOperator(M, val[key]);
	}

	struct LineOperatorConst{
		double * const val; int M;
		LineOperatorConst(int M, double * const val) :val(val), M(M){ ; }
		double operator [](int key) const {
			assert(key >= 0 && key < M);
			return val[key];
		}
		Matrix ToMat() const {
			Matrix m(1, M);
			for (int i = 0; i < M; i++)
				m.val[0][i] = val[i];
			return m;
		}
	};

	LineOperatorConst operator [] (int key) const{
		assert(key >= 0 && key < N);
		return LineOperatorConst(M, val[key]);
	}

	Matrix operator + (const double val){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] += val;
		return mm;
	}

	Matrix operator - (const double val){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] -= val;
		return mm;
	}

	Matrix operator + (const Matrix &mat){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] += mat.val[i][j];
		return mm;
	}

	Matrix operator - (const Matrix &mat){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] -= mat.val[i][j];
		return mm;
	}

	friend Matrix operator * (const double l, const Matrix &r){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = l * r.val[i][j];
		return m;
	}

	friend Matrix operator * (const Matrix &r, const double l){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = r.val[i][j] * l;
		return m;
	}

	friend Matrix operator * (const Matrix &l, const Matrix &r){
		Matrix m(l.N, r.M);
		for (int k = 0; k < l.M; k++)
			for (int i = 0; i < l.N; i++)
				for (int j = 0; j < r.M; j++)
					m.val[i][j] += l.val[i][k] * r.val[k][j];
		return m;
	}

	friend Matrix operator / (const Matrix &r, const double l){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = r.val[i][j] / l;
		return m;
	}

	friend Matrix operator / (const double l, const Matrix &r){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = l / r.val[i][j];
		return m;
	}
	void print(FILE *f = stdout, const char *fmt = "%.4lf "){
		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++)
				fprintf_s(f, fmt, val[i][j]);
			fprintf_s(f, "\n");
		}
	}

	Matrix T() const {
		Matrix m(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				m.val[i][j] = val[j][i];
		return m;
	}
	~Matrix(){
		delete[] mem;
		delete[] val;
	}
};
Matrix::Matrix(int N, int M, double v){
	this->N = N, this->M = M;
	mem = new double[N*M];
	val = new double*[N];
	memset(val, 0, sizeof(void*)*N);
	memset(mem, 0, sizeof(double)*N*M);
	double *ptr = mem;
	for (int i = 0; i < N; i++, ptr += M)
		val[i] = ptr;
	for (int i = 0; i < min(N, M); i++){
		val[i][i] = v;
	}
}

Matrix::Matrix(const Matrix &v){
	this->N = v.N, this->M = v.M;
	mem = new double[N*M];
	val = new double*[N];
	memset(val, 0, sizeof(void*)*N);
	memset(mem, 0, sizeof(double)*N*M);
	double *ptr = mem;
	for (int i = 0;  i < N; i++, ptr += M)
		val[i] = ptr;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			val[i][j] = v.val[i][j];
}
/*
	Usage:
		custom N M F -1Count Special
*/
int main(int argc, char **argv){
	FILE *fOut, *fJudge;
	srand(time(NULL));
	fopen_s(&fOut, "custom.in", "w");
	fopen_s(&fJudge, "custom.judge", "w");

	int N = atoi(argv[1]), M = atoi(argv[2]), F = atoi(argv[3]), f1 = atoi(argv[4]), spe = atoi(argv[5]);
	int id = atoi(argv[6]);
	Matrix matCustomFactor(N, F);
	Matrix matItemFactor(M, F);
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> Rand(0, 1);
	std::uniform_real_distribution<> Rand1(0.1, 1);
	std::uniform_int_distribution<> RandN(0, N - 1);
	std::uniform_int_distribution<> RandM(0, M - 1);
	std::uniform_int_distribution<> RandF(0, F - 1);
	
	if (spe == 0){
		for (int i = 0; i < N; i++)
			for (int j = 0; j < F; j++)
				matCustomFactor.val[i][j] = Rand(gen);
	} else if (spe == 1) {
		for (int i = 0; i < N; i++)
			for (int j = 0; j < F; j++)
				matCustomFactor.val[i][j] = 0; // Rand1(gen);
		for (int i = 0; i < N; i++)
			matCustomFactor.val[i][RandF(gen)] = Rand1(gen);
	}

	for (int i = 0; i < M; i++)
		for (int j = 0; j < F; j++)
			matItemFactor.val[i][j] = Rand(gen);
	Matrix matCustomItem = matCustomFactor * matItemFactor.T();
	fprintf_s(fJudge, "%d\n%d %d %d %d\n", id,N, M, F,f1);
	fprintf_s(fOut, "%d %d %d\n", N, M, F);

	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++){
			fprintf_s(fJudge, "%.10lf ", matCustomItem.val[i][j]);
		}
		fprintf_s(fJudge, "\n");
	}
	for (int i = 0; i < f1; i++)
		matCustomItem.val[RandN(gen)][RandM(gen)] = -1;
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++){
			fprintf_s(fOut, "%.10lf ", matCustomItem.val[i][j]);
		}
		fprintf_s(fOut, "\n");
	}
	if (fOut)
		fclose(fOut), fOut = 0;
	if (fJudge)
		fclose(fJudge), fJudge = 0;
	return 0;
}
