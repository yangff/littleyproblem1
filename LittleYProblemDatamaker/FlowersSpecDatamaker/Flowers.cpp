/*
Project	: Xiao Y's Hard Problems I
Author	: Yangff
Type	: Data_generator
Date	: 2014-5-15
Thanks	: http://blog.pluskid.org/?p=430
*/
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <random>
#include <ctime>

using namespace std;

const int MaxK = 10;
const int MaxN = 1000;

int Tasks[MaxK], k, nSum;

bool InRange(int v, int L, int R){
	return v >= L && v <= R;
}

struct Flower{
	double x, y;
	int kind;
} Flowers[MaxK * MaxN]; int cnt;

const double eps = 1e-8;
const double PI = 3.141592653589793238462643383279502884197169399375105;
// matrix
template<int sizeL, int sizeR>
struct Matrix{
	double m[sizeL][sizeR];
};

template<int sizeL, int sizeR, int sizeZ> Matrix<sizeL, sizeZ> operator * (const Matrix<sizeL, sizeR> &m0, const Matrix<sizeR, sizeZ> &m1){
	Matrix<sizeL, sizeZ> r;
	for (int i = 0; i < sizeL; i++)
		for (int j = 0; j < sizeZ; j++){
		r.m[i][j] = 0;
		for (int k = 0; k < sizeR; k++)
			r.m[i][j] += m0.m[i][k] * m1.m[k][j];
		}
	return r;
}

template<int sizeL, int sizeR> Matrix<sizeL, sizeR> operator + (const Matrix<sizeL, sizeR> &m0, const Matrix<sizeL, sizeR> &m1){
	Matrix<sizeL, sizeR> r;
	for (int i = 0; i < sizeL; i++)
		for (int j = 0; j < sizeR; j++)
			r.m[i][j] = m0.m[i][j] + m1.m[i][j];
	return r;
}

// Cholesky algorithm

template<int sizeL, int sizeR>
int cholesky(Matrix<sizeL, sizeR>& mat){
	int i;
	for (i = 0; i < sizeL; i++) {
		for (int k = 0; k < i; k++)
			mat.m[i][i] -= mat.m[k][i] * mat.m[k][i];
		mat.m[i][i] = sqrt(mat.m[i][i]);
		if (fabs(mat.m[i][i]) < eps)
			break;
		for (int j = i + 1; j < sizeR; j++) {
			for (int k = 0; k < i; k++)
				mat.m[i][j] -= mat.m[k][i] * mat.m[k][j];
			mat.m[i][j] /= mat.m[i][i];
		}
	}
	return i == sizeL;
}

int main(int argc, char ** args){
	srand(time(NULL));
	int _type = 0;
	if (argc == 1){
		scanf_s("%d", &k);
		/* if (_type != 1 && _type != 2){
		printf("only two type support.");
		return 4;
		} */
		_type = 1;
		if (!InRange(k, 1, 10)){
			printf("K should grater then 1 and less then 10.\n");
			return 1;
		}
		for (int i = 0; i < k; i++){
			scanf_s("%d", Tasks + i);
			if (!InRange(Tasks[i], 1, 2000)){
				printf("N_i should greater then 1 and less then 2000.\n");
				return 3;
			}
			nSum += Tasks[i];
		}
	}
	else {
		k = atoi(args[1]);
		if (!InRange(k, 1, 10)){
			printf("K should grater then 1 and less then 10.\n");
			return 1;
		}
		if (argc != k + 3){
			printf("Bad Arugments. (required %d but %d)\n", k + 2, argc);
			return 2;
		}
		_type = atoi(args[2]);
		if (_type != 1 && _type != 2){
			printf("only two type support.");
			return 4;
		}
		for (int i = 0; i < k; i++){
			Tasks[i] = atoi(args[i + 3]);
			if (!InRange(Tasks[i], 1, 2000)){
				printf("N_i should greater then 1 and less then 2000.\n");
				return 3;
			}
			nSum += Tasks[i];
		}
	}

	FILE * fdata; fopen_s(&fdata, "flowers.in", "w");
	FILE * fjudge; fopen_s(&fjudge, "flowers.judge", "w");
	std::random_device rd;
	std::mt19937 gen(rd());
	const int opA[2][2] = { { 20000, 60000 }, { 40000, 120000 } };

	std::uniform_real_distribution<> YZ(opA[_type - 1][0], opA[_type - 1][1]);
	std::uniform_real_distribution<> X(-61, 61);
	//std::uniform_int_distribution<> MU(-2000, -1000);
	std::uniform_real_distribution<> T(0, 1);

	std::uniform_int_distribution<> MO(0, 2);
	//std::uniform_real_distribution<> MU1(900, 1500);
	vector<Matrix<2, 1> > Mus;



	for (int i = -4; i <= 5; i++){
		for (int j = -4; j <= 5; j++){
			Matrix<2, 1> Mu;
			std::uniform_real_distribution<> XX(i * 2000 - 120, i * 2000 + 120);
			std::uniform_real_distribution<> YY(j * 2000 - 120, j * 2000 + 120);

			Mu.m[0][0] = XX(gen);
			Mu.m[1][0] = YY(gen);
			Mus.push_back(Mu);
		}
	}
	random_shuffle(Mus.begin(), Mus.end());
	// mu.m[0][0] = MU(gen); mu.m[1][0] = MU(gen);

	for (int i = 0; i < k; i++){
		Matrix<2, 1> mu = Mus[i];
		// random covariance
		/*
		[Y  X]
		M = [X  Z]

		*/
		Matrix<2, 2> covariance, L;
		covariance.m[0][0] = YZ(gen);
		covariance.m[1][1] = YZ(gen);
		covariance.m[0][1] = X(gen);
		covariance.m[1][0] = X(gen);
		/* printf("%lf %lf\n%lf %lf\n", covariance.m[0][0],covariance.m[0][1],
		covariance.m[1][0],covariance.m[1][1]);
		if (!cholesky(covariance)){
		puts("bad!");
		} */
#define check(x) ((x != x)?(printf("a")):(0));
		L = covariance; if (!cholesky(L)) { printf("cholesky failed!"); return 4; }
		check(L.m[0][0]); check(L.m[1][0]); check(L.m[0][1]); check(L.m[1][1]);



		for (int j = 0; j < Tasks[i]; j++){
			double T1 = T(gen), T2 = T(gen);
			double r = sqrt(-2 * log(T2));

			double theta = 2 * PI*T1;
			Matrix<2, 1> XX; XX.m[0][0] = r * cos(theta), XX.m[1][0] = r * sin(theta);

			Matrix<2, 1> X1 = mu + L * XX;
			//	check(theta);
			Flowers[cnt].x = X1.m[0][0];
			Flowers[cnt].y = X1.m[1][0];

			Flowers[cnt].kind = i + 1;
			cnt++;
		}
	}

	random_shuffle(Flowers, Flowers + cnt);

	fprintf(fjudge, "%d\n%d\n", k, cnt);
	for (int i = 0; i < cnt; i++){
		fprintf(fjudge, "%lf %lf %d\n", Flowers[i].x, Flowers[i].y, Flowers[i].kind);
	}
	fclose(fjudge);
	fprintf(fdata, "%d\n%d\n", k, cnt);
	for (int i = 0; i < cnt; i++){
		fprintf(fdata, "%lf %lf\n", Flowers[i].x, Flowers[i].y);
	}
	fclose(fdata);

}
