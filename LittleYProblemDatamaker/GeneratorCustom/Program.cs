﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorCustom
{
    class Program
    {
        // custom N M F -1Count Special
        static void MakeCustom(int id, int n, int m, int f, int f1, int spe, int iid)
        {
            Process p = new Process();
            p.StartInfo.FileName = "CustomDatamaker.exe";
            string args = n.ToString() + " " + m.ToString() + " " + f.ToString() + " " + f1.ToString() + " " + spe.ToString() + " " + iid.ToString();
            p.StartInfo.Arguments = args;
            p.Start();
            p.WaitForExit();
            if (!Directory.Exists("data"))
                Directory.CreateDirectory("data");
            if (!Directory.Exists("data/custom"))
                Directory.CreateDirectory("data/custom");
            if (p.ExitCode == 0)
            {
                if (File.Exists("data/custom/custom" + id.ToString() + ".in"))
                    File.Delete("data/custom/custom" + id.ToString() + ".in");
                File.Copy("custom.in", "data/custom/custom" + id.ToString() + ".in");

                if (File.Exists("data/custom/custom" + id.ToString() + ".judge"))
                    File.Delete("data/custom/custom" + id.ToString() + ".judge");
                File.Copy("custom.judge", "data/custom/custom" + id.ToString() + ".judge");

                File.Delete("custom.in");
                File.Delete("custom.judge");
            }
            else
            {
                Console.WriteLine("Exitcode wrong.");
            }
        }
        static void Main(string[] args)
        {
            MakeCustom(1, 50, 50, 4, 1, 0,0);

            MakeCustom(2, 50, 50, 4, 4, 0, 0);
            MakeCustom(3, 50, 50, 4, 4, 0, 0);

            MakeCustom(4, 50, 50, 4, 250, 1, 0);

            MakeCustom(5, 50, 50, 1, 250, 0, 0);

            MakeCustom(6, 5, 5, 5, 10, 0, 0);

            MakeCustom(7, 100, 110, 6, 1000, 0, 1);
            MakeCustom(8, 120, 125, 7, 1200, 0, 1);
            MakeCustom(9, 130, 135, 9, 1300, 0, 1);
            MakeCustom(10, 135, 145, 10, 1800, 0, 1);
        }
    }
}
