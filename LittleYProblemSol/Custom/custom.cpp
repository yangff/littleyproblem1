#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <cmath>
#include <cassert>
#include <cstring>
#include <type_traits>
#include <random>

using namespace std;

int n, k, m;

template<int N, int M, class R = double>
struct Matrix {
	R val[N][M];
	
	Matrix(R val = 0);
	Matrix(const R m[N][M]);
	Matrix(std::initializer_list<std::initializer_list<R > > S);
	Matrix(const Matrix<N, M, R> &v);
	template<int M, class R>
	struct LineOperator{
		R * val;
		LineOperator(R * val) :val(val){ ; }
		R & operator [] (int key){
			assert(key >= 0 && key < M);
			return val[key];
		}

		template<class T>
		void operator += (const T v){
			for (int i = 0; i < M; i++)
				val[i] += v;
		}

		template<class T>
		void operator -= (const T v){
			for (int i = 0; i < M; i++)
				val[i] -= v;
		}
		template<class T>
		void operator *= (const T v){
			for (int i = 0; i < M; i++)
				val[i] *= v;
		}

		template<class T>
		void operator /= (const T v){
			for (int i = 0; i < M; i++)
				val[i] /= v;
		}
		operator Matrix<1, M, R>() const {
			Matrix<1, M, R> m;
			for (int i = 0; i < M; i++)
				m.val[0][i] = val[i];
			return m;
		}
	};

	LineOperator<M,R> operator [] (int key){
		assert(key >= 0 && key < N);
		return LineOperator<M, R>(val[key]);
	}

	template<int M,class R>
	struct LineOperatorConst{
		R * const val;
		LineOperatorConst(R * const val) :val(val){ ; }
		R operator [](int key) const {
			assert(key >= 0 && key < M);
			return val[key];
		}
	};

	LineOperatorConst<M,R> operator [] (int key) const{
		assert(key >= 0 && key < N);
		return LineOperatorConst<M,R>(val[key]);
	}

	template<class T>
	Matrix<N,M,R> operator + (const T val){
		Matrix<N, M, R> mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] += val;
		return mm;
	}
	template<class T>
	Matrix<N, M, R> operator - (const T val){
		Matrix<N, M, R> mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] -= val;
		return mm;
	}

	Matrix<N, M, R> operator + (const Matrix<N, M, R> &mat){
		Matrix<N, M, R> mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] += mat.val[i][j];
		return mm;
	}

	Matrix<N, M, R> operator - (const Matrix<N, M, R> &mat){
		Matrix<N, M, R> mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] -= mat.val[i][j];
		return mm;
	}

	friend Matrix<N, M,R> operator * (const R l, const Matrix<N,M,R> &r){
		Matrix<N, M,R> m;
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				m.val[i][j] = l * r.val[i][j];
		return m;
	}

	friend Matrix<N, M,R> operator * (const Matrix<N, M, R> &r, const R l){
		Matrix<N, M, R> m;
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				m.val[i][j] = r.val[i][j] * l;
		return m;
	}

	template<int K>
	friend Matrix<N, K, R> operator * (const Matrix<N, M, R> &l, const Matrix<M, K, R> &r){
		Matrix<N, K, R> m;
		for (int k = 0; k < M; k++)
			for (int i = 0; i < N; i++)
				for (int j = 0; j < K; j++)
					m.val[i][j] += l.val[i][k] * r.val[k][j];
		return m;
	}

	template<class T>
	friend Matrix<N, M, R> operator / (const Matrix<N, M, R> &r, const T l){
		Matrix<N, M, R> m;
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				m.val[i][j] = r.val[i][j] / l;
		return m;
	}
	template<class T>
	friend Matrix<N, M, R> operator / (const T l, const Matrix<N, M, R> &r){
		Matrix<N, M, R> m;
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				m.val[i][j] = l/r.val[i][j];
		return m;
	}
	void print(FILE *f = stdout, const char *fmt = "%.4lf "){
		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++)
				fprintf_s(f, fmt, val[i][j]);
			fprintf_s(f, "\n");
		}
	}

	Matrix<M, N, R> T() const {
		Matrix<M, N, R> m;
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				m.val[i][j] = val[j][i];
		return m;
	}
};
template<int V>
struct _val{
	static const int value = V;
};
template<int A, int B>
struct _minval{
	typedef typename std::conditional < (A <= B), _val<A>,_val<B> >::type invoke;
};
template<int N, int M, class R>
Matrix<N, M, R>::Matrix(R v){
	memset(val, 0, sizeof(val));
	for (int i = 0; i < _minval<N, M>::invoke::value; i++){
		val[i][i] = v;
	}
}

template<int N, int M, class R>
Matrix<N, M,R>::Matrix(const R m[N][M]){
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			val[i][j] = m[i][j];
}
template<int N, int M, class R>
Matrix<N, M,R>::Matrix(std::initializer_list<std::initializer_list<R > > S){
	assert(S.size() == N);
	R *v = (R*)val;
	for (auto it = S.begin(); it != S.end(); it++){
		assert(it->size() == M);
		for (R x : *it)
			*(v++) = x;
	}
}
template<int N, int M, class R>
Matrix<N, M, R>::Matrix(const Matrix<N, M, R> &v){
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			val[i][j] = v.val[i][j];
}
template<int N, int M>
double abs(Matrix<N, M> m){
	double _abs = 0;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			_abs += m.val[i][j] * m.val[i][j];
	return _abs;
}
template<int N, int M>
Matrix<N, M> test(const Matrix<N, M> &a, const Matrix<N, M> &b){
	Matrix<N, M> ret;
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++){
			if (b.val[i][j] != -1){
				ret.val[i][j] = a.val[i][j] - b.val[i][j];
			}
		}
	}
	return ret;
}
template<int N, int M, int F>
void SVD(Matrix<N, M> matCustomItem, Matrix<N, F> &matCustomFactor, Matrix<M, F> &matItemFactor){
	// CI = CF * IF^T
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> Rand(0, 1);
	for (int i = 0; i < N; i++)
		for (int j = 0; j < F; j++)
			matCustomFactor.val[i][j] = Rand(gen);
	for (int i = 0; i < M; i++)
		for (int j = 0; j < F; j++)
			matItemFactor.val[i][j] = Rand(gen);
	double errMin = abs(test(matCustomFactor * matItemFactor.T(), matCustomItem)), oErrMin = 0;

	const double delta = 1e-5;
	const double ku = 0, km = 0;
	double lambda = 0.05;
	int cas = 0;
	do {
		
	//	Matrix<N, F> matNextCustomFactor = matCustomFactor;
		//Matrix<M, F> matNextItemFactor = matItemFactor;
		
		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++){
				if (matCustomItem.val[i][j] >= 0){
					Matrix<1, F> deltaCustomFactor = 
(matCustomItem.val[i][j] - ((Matrix<1, F>)matCustomFactor[i] * ((Matrix<1, F>)matItemFactor[j]).T()).val[0][0]) * 
(Matrix<1, F>)matItemFactor[j] - ku * (Matrix<1, F>)matCustomFactor[i];
					Matrix<1, F> deltaItemFactor = 
(matCustomItem.val[i][j] - ((Matrix<1, F>)matCustomFactor[i] * ((Matrix<1, F>)matItemFactor[j]).T()).val[0][0]) * 
(Matrix<1, F>)matCustomFactor[i] - km * (Matrix<1, F>)matItemFactor[j];
					for (int k = 0; k < F; k++){
						matCustomFactor.val[i][k] += lambda * deltaCustomFactor.val[0][k];
					}
					for (int k = 0; k < F; k++){
						matItemFactor.val[j][k] += lambda * deltaItemFactor.val[0][k];
					}
				}
			}
		}
		

		//matItemFactor = matNextItemFactor;
		//matCustomFactor = matNextCustomFactor;

		oErrMin = errMin;
		errMin = abs(test(matCustomFactor * matItemFactor.T(), matCustomItem));
		printf("%d %lf\n", ++cas, errMin);
		if (cas == 20)
			lambda /= 10.0;
		if (cas == 50)
			lambda /= 2.0;
		if (cas == 100)
			lambda /= 2.0;
	} while ( fabs(oErrMin - errMin) > delta );
}

const int detN = 1000, detM = 500, detF = 10;
Matrix<detN, detF> matCustomFactor;
Matrix<detM, detF> matItemFactor;
Matrix<detN, detM> matCustomItem;
Matrix<detN, detM> tmp;
int main(){
	srand((unsigned int)time(NULL));
	FILE *fIn = NULL, *fOut = NULL;
	fopen_s(&fIn, "custom.in", "r");
	fopen_s(&fOut, "custom.out", "w");

	

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> Rand(0, 1);
	for (int i = 0; i < detN; i++)
		for (int j = 0; j < detF; j++)
			matCustomFactor.val[i][j] = Rand(gen);
	for (int i = 0; i < detM; i++)
		for (int j = 0; j < detF; j++)
			matItemFactor.val[i][j] = Rand(gen);

	matCustomItem = matCustomFactor * matItemFactor.T();
	tmp = matCustomItem;
	
	 for (int i = 0; i < 5000; i++){
		int X = rand() % detN;
		int Y = rand() % detM;
		matCustomItem[X][Y] = -1;
	} 
	
	SVD(matCustomItem, matCustomFactor, matItemFactor);
	printf("%.4lf\n", abs(test(matCustomFactor * matItemFactor.T(), tmp)));
	system("pause");
	if (fIn) fclose(fIn); if (fOut) fclose(fOut);
	return 0;
}
