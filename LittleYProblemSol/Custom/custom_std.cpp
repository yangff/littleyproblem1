#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <cmath>
#include <cassert>
#include <cstring>
#include <type_traits>
#include <random>

using namespace std;

int n, k, m;

struct Matrix {
	int N, M;
	double **val, *mem;
	Matrix(int N, int M, double val = 0);
	Matrix(const Matrix &v);
	struct LineOperator{
		double * val; int M;
		LineOperator(int M, double * val) :val(val), M(M){ ; }
		double & operator [] (int key){
			assert(key >= 0 && key < M);
			return val[key];
		}

		void operator += (const double v){
			for (int i = 0; i < M; i++)
				val[i] += v;
		}

		void operator -= (const double v){
			for (int i = 0; i < M; i++)
				val[i] -= v;
		}

		void operator *= (const double v){
			for (int i = 0; i < M; i++)
				val[i] *= v;
		}

		void operator /= (const double v){
			for (int i = 0; i < M; i++)
				val[i] /= v;
		}
		Matrix ToMat() {
			Matrix m(1, M);
			for (int i = 0; i < M; i++)
				m.val[0][i] = val[i];
			return m;
		}
	};

	LineOperator operator [] (int key){
		assert(key >= 0 && key < N);
		return LineOperator(M, val[key]);
	}

	struct LineOperatorConst{
		double * const val; int M;
		LineOperatorConst(int M, double * const val) :val(val), M(M){ ; }
		double operator [](int key) const {
			assert(key >= 0 && key < M);
			return val[key];
		}
		Matrix ToMat() const {
			Matrix m(1, M);
			for (int i = 0; i < M; i++)
				m.val[0][i] = val[i];
			return m;
		}
	};

	LineOperatorConst operator [] (int key) const{
		assert(key >= 0 && key < N);
		return LineOperatorConst(M, val[key]);
	}

	Matrix operator + (const double val){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] += val;
		return mm;
	}

	Matrix operator - (const double val){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] -= val;
		return mm;
	}

	Matrix operator + (const Matrix &mat){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] += mat.val[i][j];
		return mm;
	}

	Matrix operator - (const Matrix &mat){
		Matrix mm(*this);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				mm.val[i][j] -= mat.val[i][j];
		return mm;
	}

	friend Matrix operator * (const double l, const Matrix &r){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = l * r.val[i][j];
		return m;
	}

	friend Matrix operator * (const Matrix &r, const double l){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = r.val[i][j] * l;
		return m;
	}

	friend Matrix operator * (const Matrix &l, const Matrix &r){
		Matrix m(l.N, r.M);
		for (int k = 0; k < l.M; k++)
			for (int i = 0; i < l.N; i++)
				for (int j = 0; j < r.M; j++)
					m.val[i][j] += l.val[i][k] * r.val[k][j];
		return m;
	}

	friend Matrix operator / (const Matrix &r, const double l){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = r.val[i][j] / l;
		return m;
	}

	friend Matrix operator / (const double l, const Matrix &r){
		Matrix m(r.N, r.M);
		for (int i = 0; i < r.N; i++)
			for (int j = 0; j < r.M; j++)
				m.val[i][j] = l / r.val[i][j];
		return m;
	}
	void print(FILE *f = stdout, const char *fmt = "%.4lf "){
		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++)
				fprintf_s(f, fmt, val[i][j]);
			fprintf_s(f, "\n");
		}
	}

	Matrix T() const {
		Matrix m(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				m.val[i][j] = val[j][i];
		return m;
	}
	~Matrix(){
		delete[] mem;
		delete[] val;
	}
};
Matrix::Matrix(int N, int M, double v){
	this->N = N, this->M = M;
	mem = new double[N*M];
	val = new double*[N];
	memset(val, 0, sizeof(void*)*N);
	memset(mem, 0, sizeof(double)*N*M);
	double *ptr = mem;
	for (int i = 0; i < N; i++, ptr += M)
		val[i] = ptr;
	for (int i = 0; i < min(N, M); i++){
		val[i][i] = v;
	}
}

Matrix::Matrix(const Matrix &v){
	this->N = v.N, this->M = v.M;
	mem = new double[N*M];
	val = new double*[N];
	memset(val, 0, sizeof(void*)*N);
	memset(mem, 0, sizeof(double)*N*M);
	double *ptr = mem;
	for (int i = 0; i < N; i++, ptr += M)
		val[i] = ptr;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			val[i][j] = v.val[i][j];
}

double abs(const Matrix &m){
	double _abs = 0;
	for (int i = 0; i < m.N; i++)
		for (int j = 0; j < m.M; j++)
			_abs += m.val[i][j] * m.val[i][j];
	return _abs;
}

Matrix test(const Matrix &a, const Matrix &b){
	Matrix ret(a.N, a.M);
	for (int i = 0; i < a.N; i++){
		for (int j = 0; j < a.M; j++){
			if (b.val[i][j] != -1){
				ret.val[i][j] = a.val[i][j] - b.val[i][j];
			}
		}
	}
	return ret;
}

void SVD(int N,int M,int F,const Matrix &matCustomItem, Matrix &matCustomFactor, Matrix &matItemFactor){
	// CI = CF * IF^T
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> Rand(0, 1);
	for (int i = 0; i < N; i++)
		for (int j = 0; j < F; j++)
			matCustomFactor.val[i][j] = Rand(gen);
	for (int i = 0; i < M; i++)
		for (int j = 0; j < F; j++)
			matItemFactor.val[i][j] = Rand(gen);
	double errMin = abs(test(matCustomFactor * matItemFactor.T(), matCustomItem)), oErrMin = 0;

	const double delta = 1e-5;
	const double ku = 0, km = 0;
	double lambda = 0.05;
	int cas = 0;
	do {

		//	Matrix<N, F> matNextCustomFactor = matCustomFactor;
		//Matrix<M, F> matNextItemFactor = matItemFactor;

		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++){
				if (matCustomItem.val[i][j] >= 0){
					Matrix deltaCustomFactor =
						(matCustomItem.val[i][j] - (matCustomFactor[i].ToMat() * (matItemFactor[j].ToMat()).T()).val[0][0]) *
						matItemFactor[j].ToMat() - ku * matCustomFactor[i].ToMat();
					Matrix deltaItemFactor =
						(matCustomItem.val[i][j] - (matCustomFactor[i].ToMat() * (matItemFactor[j].ToMat()).T()).val[0][0]) *
						matCustomFactor[i].ToMat() - km * matItemFactor[j].ToMat();
					for (int k = 0; k < F; k++){
						matCustomFactor.val[i][k] += lambda * deltaCustomFactor.val[0][k];
					}
					for (int k = 0; k < F; k++){
						matItemFactor.val[j][k] += lambda * deltaItemFactor.val[0][k];
					}
				}
			}
		}


		//matItemFactor = matNextItemFactor;
		//matCustomFactor = matNextCustomFactor;

		oErrMin = errMin;
		errMin = abs(test(matCustomFactor * matItemFactor.T(), matCustomItem));
		// printf("%d %lf\n", ++cas, errMin);
		if (cas == 20)
			lambda /= 10.0;
		/*if (cas == 50)
			lambda /= 2.0;
		if (cas == 100)
			lambda /= 2.0;*/
	} while (fabs(oErrMin - errMin) > delta);
}

//const int detN = 1000, detM = 500, detF = 10;

int main(){
	srand((unsigned int)time(NULL));
	FILE *fIn = NULL, *fOut = NULL;
	fopen_s(&fIn, "custom.in", "r");
	fopen_s(&fOut, "custom.out", "w");
	int detN = 1000, detM = 500, detF = 10;
	fscanf_s(fIn, "%d%d%d", &detN, &detM, &detF);
	Matrix matCustomFactor(detN, detF);
	Matrix matItemFactor(detM, detF);
	Matrix matCustomItem(detN, detM);
	for (int i = 0; i < detN; i++){
		for (int j = 0; j < detM; j++){
			double x; fscanf_s(fIn, "%lf", &x);
			matCustomItem.val[i][j] = x;
		}
	}
	SVD(detN,detM,detF,matCustomItem, matCustomFactor, matItemFactor);
	Matrix t = matCustomFactor * matItemFactor.T();
	for (int i = 0; i < detN; i++){
		for (int j = 0; j < detM; j++){
			fprintf_s(fOut, "%lf ", t.val[i][j]);
		}
		fprintf_s(fOut, "\n");
	}
	if (fIn) fclose(fIn); if (fOut) fclose(fOut);
	return 0;
}
