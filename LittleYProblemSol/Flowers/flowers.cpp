﻿#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>

using namespace std;

struct PointF{
	double x, y;
	PointF(double x = 0, double y = 0) :x(x), y(y){ ; }
};

double dist(PointF x, PointF y){
	return (x.x - y.x) * (x.x - y.x) + (x.y - y.y) * (x.y - y.y);
}

const double eps = 1e-8;
const int MaxK = 10;
const int MaxR = 10;
const int MaxN = 1000 * MaxK;

PointF center[MaxK];
PointF points[MaxN];

int n, k;

void InitCenter(){
	static int cnt[MaxK];
	memset(cnt, 0, sizeof(cnt));
	for (int i = 0; i < n; i++){
		int id = rand() % k;
		cnt[id]++;
		center[id].x += points[i].x;
		center[id].y += points[i].y;
	}
	for (int i = 0; i < k; i++)
		center[i].x /= cnt[i], center[i].y /= cnt[i];
}

int klass[MaxN];

void classify(){
	for (int i = 0; i < n; i++){
		int idx = 0; double val = dist(points[i], center[0]);
		for (int j = 1; j < k; j++){
			double d = dist(points[i], center[j]);
			if (d < val){
				val = d;
				idx = j;
			}
		}
		klass[i] = idx;
	}
}

bool calcCenter(){
	bool halt = true;
	static int cnt[MaxK];
	memset(cnt, 0, sizeof(cnt));

	static PointF sum[MaxK];
	memset(sum, 0, sizeof(sum));

	for (int i = 0; i < n; i++){
		int id = klass[i];
		cnt[id]++; 
		sum[id].x += points[i].x;
		sum[id].y += points[i].y;
	}

	for (int i = 0; i < k; i++){
		if (cnt[i] != 0){
			sum[i].x /= cnt[i];
			sum[i].y /= cnt[i];
		} else {
			int a = (i + 1) % k;
			int b = (i + 3) % k;
			int c = (i + 5) % k;
			sum[i].x = (center[a].x + center[b].x + center[c].x) / 3.0;
			sum[i].y = (center[a].y + center[b].y + center[c].y) / 3.0;
		}
	}

	for (int i = 0; i < k; i++){
		if (sqrt(dist(sum[i], center[i])) >= eps){
			halt = false;
			break;
		}
	}

	if (!halt){
		for (int i = 0; i < k; i++){
			center[i].x = sum[i].x;
			center[i].y = sum[i].y;
		}
	}

	return halt;
}

double calcSati(){
	double sati = 0;

	static int cnt[MaxK];
	memset(cnt, 0, sizeof(cnt));

	static double ss[MaxK];
	memset(ss, 0, sizeof(ss));

	for (int i = 0; i < n; i++){
		int id = klass[i];
		cnt[id]++;
		ss[id] += dist(center[id], points[i]);
	}
	for (int i = 0; i < k; i++)
		sati += cnt[i] * ss[i];
	return sati;
}

double runk; int cls[MaxN];

void work(){
	InitCenter();
	classify();
	while (!calcCenter()){
		classify();
	}
}

void solve(){
	runk = 1e100;
	for (int T = 0; T < 300; T++){
		work();
		double ss = calcSati();
		if (ss < runk){
			runk = ss;
			for (int i = 0; i < n; i++){
				cls[i] = klass[i];
			}
		}
	}
}

int main(){
	srand(time(NULL));
	FILE *nfIn, *nfOut;
	fopen_s(&nfIn, "flowers.in", "r");
	fopen_s(&nfOut, "flowers.out", "w");
	fscanf_s(nfIn,"%d%d", &k, &n);
	for (int i = 0; i < n; i++){
		fscanf_s(nfIn,"%lf%lf", &points[i].x, &points[i].y);
	}
	
	solve();
	for (int i = 0; i < n; i++){
	//	_ASSERT(cls[i] >= 0);
		fprintf(nfOut, "%d\n", cls[i] + 1);
	}
	fclose(nfIn); fclose(nfOut);
}
