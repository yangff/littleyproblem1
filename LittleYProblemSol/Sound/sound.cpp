#include <cstdio>
#include <complex>
using namespace std;
const int MaxT = 1000, MaxW = 100;
bool sound_map[MaxW + 1][MaxT];
double value[2000];
complex<double> f[2000];
const double pi = 3.1415926535897932384626433832795;
double a[2000];
int main(){
	FILE *nfIn, *nfOut;
	freopen_s(&nfIn, "sound.in", "r", stdin);
	freopen_s(&nfOut, "sound.out", "w", stdout);
	
	int T, W; scanf_s("%d%d", &T, &W);
	for (int i = 0; i < T; i++){
		for (int j = 0; j < 2000; j++){
			scanf_s("%lf", &value[j]);
		}
			for (int j = 1; j <= W; j++){
				f[j] = 0;
			for (int k = 0; k < 2000; k++)
				f[j] += value[k] * complex<double>(cos((2 * pi * k * j) / 2000.0), sin((2 * pi * k * j) / 2000.0));
			a[j] = abs(f[j]);
			if (a[j] > 1e-4)
				sound_map[j][i] = 1;
		}
	}
	for (int i = 1; i <= W; i++){
		for (int j = 0; j < T; j++)
			printf("%d ", sound_map[i][j]);
		puts("");
	}
}
