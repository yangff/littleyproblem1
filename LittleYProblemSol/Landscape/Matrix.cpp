#include "Matrix.h"
#include <cmath>
#include <algorithm>
using namespace std;
const double eps = 1e-8;
Matrix Matrix::T() const {
	Matrix ret(h, w);
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			ret.get(j, i) = val[i][j];
	return ret;
}

Matrix Matrix::prod(const Matrix & r) const {
	assert(w == r.h);
	const Matrix transp(r.T());
	Matrix ret(r.w, h);
	for (int i = 0; i < h; i++)
		for (int j = 0; j < r.w; j++)
			for (int k = 0; k < w; k++)
		ret.get(i, j) += val[i][k] * transp.get(j, k);
	return ret;
}

bool Matrix::Inv(Matrix &ret) const{
	ret = Matrix::I(ret.w);
	Matrix z = *this;
	for (int i = 0; i < h - 1; i++){
		int x = i + 1;
		for (int j = i + 2; j < h; j++){
			if (fabs(z.val[j][i]) > fabs(z.val[x][i]))
				x = j;
		}
		if (fabs(z.val[x][i]) < eps)
			return false;
		swap(z.val[x], z.val[i]);
		swap(ret.val[x], ret.val[i]);

		for (int j = i + 1; j < h; j++){
			double d = z.val[j][i] / z.val[i][i];
			for (int k = 0; k < w; k++){
				z.val[j][k] = z.val[j][k] - z.val[i][k] * d;
			}
			for (int k = 0; k < w; k++){
				ret.val[j][k] = ret.val[j][k] - ret.val[i][k] * d;
			}
		}
	}

	for (int i = 0; i < w; i++)
		if (fabs(z.val[i][i]) < eps))
			return false;
	
	return true;
}

bool Matrix::solve_overdetermined(Matrix & x, const Matrix & b) const {
	assert(h >= w);
	Matrix mt = T();
	Matrix mtm = mt.prod(*this);
	Matrix inverse(mtm.w, mtm.h);
	if (!mtm.Inv(inverse))
		return false;
	x = inverse.prod(mt).prod(b);
	return true;
}
