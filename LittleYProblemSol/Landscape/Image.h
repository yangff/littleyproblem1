#pragma once
class Image
{
protected:
	static const int MaxW = 2000, MaxH = 2000;
public:
	Image(FILE *fImg);
	~Image();

public:
	int mat[MaxH][MaxW];
	int H, W;
};

