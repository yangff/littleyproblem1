#pragma once
#include <cstring>
#include <cassert>

class Matrix
{
public:
	double **val;
	int w, h;

	void allocval(){
		val = new double *[h];
		for (int i = 0; i < h; i++)
			val[i] = new double[w];
	}

	Matrix(int w, int h):w(w),h(h){
		allocval();
	}

	void freeval(){
		for (int i = 0; i < h; i++)
			delete[] val[i];
		delete[] val;
	}
	
	~Matrix(){
		freeval();
	}

	Matrix & operator = (const Matrix &m){
		if (this != &m){
			freeval();
			w = m.w, h = m.h;
			allocval();
			for (int i = 0; i < h; i++){
				for (int j = 0; j < w; j++)
					val[i][j] = m.val[i][j];
			}
		}
		return *this;
	}

	Matrix(const Matrix &r) {
		*this = r;
	}

	double & get(int i, int j){
		return val[i][j];
	}

	const double & get(int i, int j) const {
		return val[i][j];
	}

	bool Inv(Matrix & ret) const;

	Matrix T() const;

	Matrix prod(const Matrix &r) const;

	bool solve_overdetermined(Matrix & x, const Matrix & b) const;

	bool SVD(Matrix & u, Matrix & s, Matrix & v) const;

	void normrot();

	double sqrsum() const;

	Matrix col(int i) const;

	static Matrix I(int c);

};

